<h2>Introduction</h2>
<p>
	The Nutscript Reskins plugin is a addition to Nutscript enabled Garry's Mod servers. This plugin changes the look of the default Nutscript
	'characters' menu.
</p>

<h2>Installation</h2>
<p>
	1. Extract the 'Nutscript_Reskins' folder from the downlaoded .zip file <br>
	2. Put the 'Nutscript_Reskins' folder inside garrysmod>gamemodes>yourschema>plugins folder.<br>
	3. Enjoy!<br>
</p>
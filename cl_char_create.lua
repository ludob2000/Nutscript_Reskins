local PANEL = {}

function PANEL:Init()
  self:SetSize(ScrW(), ScrH())
  self:SetZPos(20)
  self:SetTitle("")
  self:Center()
  self:MakePopup()
  self:ShowCloseButton(false)

  local function postDark()
    for k,v in pairs(self:GetChildren()) do
      v:SetZPos(100)
    end

    local title = self:Add("DLabel")
    title:SetText("Character Creation")
    title:SetFont("nutBigFont")
    title:SetPos(90,10)
    title:SetColor(color_white)
    title:SizeToContents()

    local function notif(txt, callback)
      if self.notif and IsValid(self.notif) then self.notif:Remove() end

      local x,y = title:GetPos()
      self.notif = self:Add("DPanel")
      self.notif:SetPos(x + title:GetWide() + 20, y)
      self.notif:SetAlpha(0)
      self.notif:AlphaTo(255, 0.2)
      self.notif:SetTall(title:GetTall())
      function self.notif:Paint(w,h)
        draw.RoundedBox(4,0,0,w,h,Color(60,60,60))
      end

      self.notif.text = self.notif:Add("DLabel")
      self.notif.text:SetText(txt)
      self.notif.text:SetFont("nutMediumFont")
      self.notif.text:SizeToContents()

      self.notif:SetWide(self.notif.text:GetWide()+20)
      self.notif.text:Center()

      local init = Derma_Anim("init_notif", self.notif, function(pnl,anim,dt,data)
        pnl:SetWide(0 + (data * dt^2))
      end)
      init:Start(0.2, self.notif:GetWide())
      function self.notif:Think()
        if init:Active() then
          init:Run()
        end
      end

      --Fade out
      timer.Simple(3, function()
        self.notif:AlphaTo(0,0.2,0,function()
          self.notif:Remove()

          if callback then callback() end --Call the callback
        end)
      end)
    end

    local back = self:Add("DButton")
    back:SetText("<- Back")
    back:SetSize(70,30)
    back:SetFont("nutSmallFont")
    back:SetPos(10,10 + back:GetTall()/6)
    function back:Paint(w,h)
      draw.RoundedBox(4,0,0,w,h,Color(38,38,38))
    end
    function back.DoClick()
      self.darkness:SetZPos(999)
      self.darkness:AlphaTo(255,0.8,0,function()
        self:Remove()

        local dark = vgui.Create("DPanel")
        dark:SetSize(ScrW(),ScrH())
        dark:Center()
        dark:SetAlpha(255)
        dark:AlphaTo(0,0.8,0.5,function()
          dark:Remove(0)
        end)
        function dark:Paint(w,h)
          draw.RoundedBox(0,0,0,w,h,Color(0,0,0))
        end
      end)
    end

    local function darken(col)
      return Color(col.r-20, col.g-20, col.b-20)
    end

    local function showCharInfo()
      self.char_info = self:Add("DPanel")
      self.char_info:SetSize(self:GetWide(), self:GetTall()*0.3+35)
      self.char_info:SetPos(0,self:GetTall()-self.char_info:GetTall())
      function self.char_info:Paint(w,h)
        draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
      end

      local function moveBelow(pnl, tp)
        local x,y = tp:GetPos()

        pnl:SetPos(x,y+40)
      end

      local function moveRight(pnl, tp)
        local x,y = tp:GetPos()

        pnl:SetPos(x + tp:GetWide() + 30, y - pnl:GetTall()/4)
      end

      self.char_info.title = self.char_info:Add("DLabel")
      self.char_info.title:SetText("Character Info")
      self.char_info.title:SetFont("nutMediumFont")
      self.char_info.title:SetColor(color_white)
      self.char_info.title:SizeToContents()
      self.char_info.title:SetPos(20,10)

      --Infos
      self.char_info.fname = self.char_info:Add("DLabel")
      self.char_info.fname:SetText("First Name: ")
      self.char_info.fname:SetFont("nutSmallFont")
      self.char_info.fname:SetColor(color_white)
      self.char_info.fname:SizeToContents()
      self.char_info.fname:SetPos(30,60)
      self.char_info.fname.get_te = true

      self.char_info.lname = self.char_info:Add("DLabel")
      self.char_info.lname:SetText("Last Name: ")
      self.char_info.lname:SetFont("nutSmallFont")
      self.char_info.lname:SetColor(color_white)
      self.char_info.lname:SizeToContents()
      self.char_info.lname.get_te = true
      moveBelow(self.char_info.lname, self.char_info.fname)

      self.char_info.desc = self.char_info:Add("DLabel")
      self.char_info.desc:SetText("Description: ")
      self.char_info.desc:SetFont("nutSmallFont")
      self.char_info.desc:SetColor(color_white)
      self.char_info.desc:SizeToContents()
      self.char_info.desc:SetPos(300,50)

      self.char_info.desc.te = self.char_info:Add("DTextEntry")
      self.char_info.desc.te:SetSize(180,45)
      self.char_info.desc.te:SetMultiline(true)
      self.char_info.desc.te:SetPos(300,70)

      --Create TextEntries
      local entries = {}
      for k,p in pairs(self.char_info:GetChildren()) do
        if p.get_te then
          local te = self.char_info:Add("DTextEntry")
          te:SetSize(120,25)
          entries[string.Split(p:GetValue(),":")[1]:lower()] = te
          moveRight(te, p)
        end
      end

      --Creating playermodel list
      function self.char_info:ShowPlayermodels()
        if self.model_scroll and IsValid(self.model_scroll) then
          self.model_scroll:Remove()
        end

        self.model_scroll = self:Add("DScrollPanel")
        self.model_scroll:SetSize(self:GetWide(),self:GetTall()*0.45)
        self.model_scroll:SetPos(0,self:GetTall()-self.model_scroll:GetTall())
        self.model_list = self.model_scroll:Add("DIconLayout")
        self.model_list:SetSize(self.model_scroll:GetSize())
        function self.model_list:Paint(w,h)
          draw.RoundedBox(0,0,0,w,h,Color(45,45,45))
        end

        for k,v in pairs(factionsToShow[activeFactionID].models) do
          local p = self.model_list:Add("DPanel")
          p:SetSize(self.model_list:GetWide()/10,self.model_list:GetTall())
          p.childHovered = false
          function p.Paint(this, w,h)
            if p:IsHovered() or this.childHovered or modelpnls[activeFactionID]:GetModel() == v then
              draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
            end
          end

          local mdl = p:Add("DModelPanel")
          mdl:SetSize(p:GetSize())
          mdl:SetModel(v)
          mdl.Entity:SetAngles(Angle(0,45,0))
          mdl.Entity:ResetSequence(2)
          function mdl:LayoutEntity() end

          local b = p:Add("DButton")
          b:SetSize(p:GetSize())
          b:SetText("")
          function b.DoClick()
            modelpnls[activeFactionID]:SetModel(v)
          end
          function b:Paint() end
          function b:Think()
            if self:IsHovered() then
              p.childHovered = true
            else
              p.childHovered = false
            end
          end
        end
      end

      --Create Button
      self.char_info.create = self.char_info:Add("DButton")
      self.char_info.create:SetSize(150,50)
      self.char_info.create:SetText("Create Character")
      self.char_info.create:SetFont("nutSmallFont")
      self.char_info.create:SetColor(color_white)
      self.char_info.create:SetPos(self.char_info:GetWide() - self.char_info.create:GetWide()-80,0)
      self.char_info.create:CenterVertical(0.25)
      function self.char_info.create:TweenWidthTo(w,way,callback)
        if not w then return end

        local tween = Derma_Anim("TweenWidth", self, function(pnl,anim,dt,data)
          if way == 1 then
            pnl:SetWide(data[3] - w * dt^2)
            pnl:SetPos(data[1] + w * dt^2, data[2])
          elseif way == 2 then
            pnl:SetWide(data[3] + w * dt^2)
            pnl:SetPos(data[1] - w * dt^2, data[2])
          end
        end)

        local x,y = self:GetPos()
        tween:Start(0.2, {x,y,self:GetWide()})
        function self:Think()
          if tween:Active() then
            tween:Run()
          end
        end

        if callback then
          timer.Simple(0.2, function()
            callback()
          end)
        end
      end
      function self.char_info.create.DoClick(this)
        this:SetDisabled(true)

        local payload = {}
        payload.name = entries["first name"]:GetValue() .. " " .. entries["last name"]:GetValue()
        payload.desc = self.char_info.desc.te:GetValue()
        payload.faction = factionsToShow[activeFactionID].realID
        payload.model = modelpnls[activeFactionID].id
        payload.data = {}
        payload.steamID = LocalPlayer():SteamID64()

        for k,v in SortedPairsByMemberValue(nut.char.vars, "index") do --Checking the variables
          local value = payload[k]

          if v.onValidate then
            local result = {v.onValidate(value, payload, LocalPlayer())}

            if result[1] == false then
              notif(L(unpack(result,2)), function()
                this:SetDisabled(false)
              end)
              return
            end
          end
        end

        netstream.Start("charCreate", payload)
        netstream.Hook("charAuthed", function(fault, ...) --Check for server response
          if type(fault) == "string" then
            notif(L(fault,...))
            return
          end

          if type(fault) == "table" then --Updating the characters table
            nut.characters = fault
          end

          netstream.Start("charChoose", nut.characters[#nut.characters]) --Choose latest character
          this:SetDisabled(true)

          self.darkness:SetZPos(999)
          self.darkness:AlphaTo(255,1,1,function()
            self.darkness:Remove()
            self.loadChar:Remove()

            local dark = vgui.Create("DPanel")
            dark:SetSize(ScrW(),ScrH())
            dark:Center()
            dark:AlphaTo(0,1,1,function()
              dark:Remove()
            end)
            function dark:Paint(w,h)
              draw.RoundedBox(0,0,0,w,h,Color(0,0,0))
            end
          end)
        end)
      end
      function self.char_info.create:Paint(w,h)
        if self:IsHovered() then
          draw.RoundedBox(4,0,0,w,h,factionsToShow[activeFactionID].color)
        else
          draw.RoundedBox(4,0,0,w,h,Color(38,38,38))
        end
      end
    end

    --Show Factions
    factionsToShow = {}
    modelpnls = {}
    activeFactionID = 1
    showCharInfo()
    for k,fac in pairs(nut.faction.indices) do

      if (fac.isPublic) or (fac.isPublic == nil) then --Checking if faction is public
        fac.realID = k
        factionsToShow[#factionsToShow+1] = fac
      end
    end

    local modelcount = table.Count(factionsToShow)

    --Making playermodel panels
    local mdlscroll = self:Add("DScrollPanel")
    mdlscroll:SetSize(self:GetWide(), self:GetTall() - (self:GetTall() * 0.4) - 60)
    mdlscroll:SetPos(0, 60)
    local mdllist = mdlscroll:Add("DIconLayout")
    mdllist:SetSize(mdlscroll:GetSize())
    mdllist:SetSpaceX(5)

    local grad = nut.util.getMaterial("vgui/gradient-u")
    for k,fac in pairs(factionsToShow) do
      local panel = mdllist:Add("DPanel")
      panel:SetSize((mdllist:GetWide()/modelcount)-5, mdllist:GetTall())
      panel.activeFacIsUs = false
      function panel:Paint(w,h)
        if self.paintBackground then
          draw.RoundedBox(0,0,0,w,h,Color(38,38,38))
          draw.RoundedBox(0,0,0,w,4,fac.color)
        end
      end
      function panel:Think()
        if activeFactionID == k then
          self.title:SetVisible(true)
          self.paintBackground = true
        else
          self.title:SetVisible(false)
          self.paintBackground = false
        end
      end

      panel.title = panel:Add("DLabel")
      panel.title:SetText(fac.name)
      panel.title:SetFont("nutMediumFont")
      panel.title:SetColor(color_white)
      panel.title:SetPos(15,15)
      panel.title:SizeToContents()

      local pm = panel:Add("DModelPanel")
      pm:SetSize(panel:GetWide(),panel:GetTall())
      pm:CenterHorizontal()
      pm:SetModel(fac.models[math.random(1, #fac.models)])
      pm:SetFOV(100)
      pm.id = k

      local boneid = pm.Entity:LookupBone("ValveBiped.Bip01_Head1")
      print(boneid)
      -- if boneid and IsValid then
      --   local head = pm.Entity:GetBonePosition()
      --   pm:SetLookAt(head - Vector(0,0,30))
      -- end
      function pm:LayoutEntity(ent)
        ent:SetSequence(2)
        ent:SetAngles(Angle(0,45,0))
        self:RunAnimation()
      end

      modelpnls[k] = pm
    end

    --Drawing panels
    local facName = self:Add("DLabel")
    facName:SetText(factionsToShow[activeFactionID].name)
    facName:SetFont("nutMediumFont")
    facName:SetColor(color_white)
    facName:SetPos(0,70+mdlscroll:GetTall())
    facName:SizeToContents()
    facName:CenterHorizontal()
    function facName:PerformLayout(w,h)
      facName:SetPos(0,70+mdlscroll:GetTall())
      facName:SizeToContents()
      facName:CenterHorizontal()
    end
    function facName:Update()
      self:SetText(factionsToShow[activeFactionID].name)
      self:InvalidateLayout()
    end

    local activeFacPanel = mdlscroll:Add("DPanel")
    activeFacPanel:SetSize(mdlscroll:GetWide()/3, mdlscroll:GetTall())
    activeFacPanel:SetPos(mdlscroll:GetWide()/2 - activeFacPanel:GetWide()/2,0)
    function activeFacPanel:Paint(w,h) end

    if modelcount > 1 then --Enable next/prev buttons if there's more than 1 faction to show
      local x,y = facName:GetPos()

      local next = self:Add("DButton")
      next:SetText(">")
      next:SetFont("nutSmallFont")
      next:SetColor(color_white)
      function next:PerformLayout()
        local x,y = facName:GetPos()
        next:SetPos(x+facName:GetWide()+15,y+next:GetTall()/4)
      end
      function next:Paint(w,h)
        if self:IsHovered() then
          draw.RoundedBoxEx(4,0,0,w,h,Color(45,45,45),false,true,false,true)
        else
          draw.RoundedBoxEx(4,0,0,w,h,Color(40,40,40),false,true,false,true)
        end
      end
      next:InvalidateLayout()

      local prev = self:Add("DButton")
      prev:SetText("<")
      prev:SetFont("nutSmallFont")
      prev:SetColor(color_white)
      function prev:PerformLayout()
        local x,y = facName:GetPos()
        prev:SetPos(x-self:GetWide()-15,y+prev:GetTall()/4)
      end
      function prev:Paint(w,h)
        if self:IsHovered() then
          draw.RoundedBoxEx(4,0,0,w,h,Color(45,45,45),true,false,true,false)
        else
          draw.RoundedBoxEx(4,0,0,w,h,Color(40,40,40),true,false,true,false)
        end
      end
      prev:InvalidateLayout()


      function next.DoClick()
        if not (modelcount > activeFactionID) then return end
        activeFactionID = activeFactionID + 1
        facName:Update()

        self.char_info:ShowPlayermodels()
        facName:InvalidateLayout()
        self:InvalidateLayout()
        prev:InvalidateLayout()
      end
      function prev.DoClick()
        if not (activeFactionID > 1) then return end
        activeFactionID = activeFactionID - 1
        facName:Update()

        self.char_info:ShowPlayermodels()
        facName:InvalidateLayout()
        self:InvalidateLayout()
        next:InvalidateLayout()
      end
    end
    self.char_info:ShowPlayermodels()
  end

  self.darkness = self:Add("DPanel") --The darkside is among us
  self.darkness:SetSize(self:GetSize())
  self.darkness:SetZPos(200)
  self.darkness:SetAlpha(0)
  self.darkness:AlphaTo(255, 1, 0, function()
    postDark()
    self.theNightHasPassed = true
    self.darkness:AlphaTo(0,0.8,0.5,function()
        self.darkness:SetZPos(-1)
    end)
  end)
  function self.darkness:Paint(w,h)
    draw.RoundedBox(0,0,0,w,h,Color(0,0,0))
  end
end

function PANEL:SetBackPanel(pnl)
  self.loadChar = pnl
end

function PANEL:Paint(w,h)
  if self.theNightHasPassed then --Draw only when the darkness is removed
    draw.RoundedBox(0,0,0,w,h,Color(35,35,35))
  end
end

vgui.Register("nutCharCreateReskined",PANEL,"DFrame")

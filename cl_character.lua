local PANEL = {}
	function PANEL:Init()
		local fadeSpeed = 1
    local nut_color = nut.config.get("color")

    --Removing loading/error screen
		if (IsValid(nut.gui.loading)) then
			nut.gui.loading:Remove()
		end

    --Playing music
		if not nut.localData.intro then
			timer.Simple(0.1, function()
				vgui.Create("nutIntro", self)
			end)
		end

    --Check if player came from F1 Menu
		if (IsValid(nut.gui.char) or (LocalPlayer().getChar and LocalPlayer():getChar())) then
			nut.gui.char:Remove()
			fadeSpeed = 0
		end

    --Check for keypress
    function self:OnKeyCodePressed(key)
      if LocalPlayer():getChar() and key == KEY_F1 then
        if self.darkness and IsValid(self.darkness) then
          self.darkness:SetZPos(999)
          self.darkness:AlphaTo(255,0.2,0,function()
            self:Remove()

            local darkness = vgui.Create("DPanel")
            darkness:SetZPos(999)
            darkness:SetSize(ScrW(), ScrH())
            darkness.Paint = function(this, w, h)
              surface.SetDrawColor(0, 0, 0)
              surface.DrawRect(0, 0, w, h)
            end
            darkness:AlphaTo(0,0.2,0,function()
              darkness:Remove()
            end)
          end)
        end
      end
    end

		nut.gui.char = self

    --Recenter this shit
		self:SetSize(ScrW(),ScrH())
		self:MakePopup()
		self:Center()
		self:ParentToHUD()

    --Background fade out
		self.darkness = self:Add("DPanel")
		self.darkness:Dock(FILL)
		self.darkness.Paint = function(this, w, h)
			draw.RoundedBox(0,0,0,w,h,Color(0,0,0))
		end
		self.darkness:SetZPos(99)

    --Button Tables & vars
    self.btns = {}
    self.btns.list = {}
    self.btns.nodes = {}

    self.btns.parent = self:Add("DPanel")
    self.btns.parent:SetSize(ScrW()*.2,ScrH())
    function self.btns.parent:Paint(w,h)
      draw.RoundedBox(0,0,0,w,h,Color(25,25,25))
    end

		self.btns.parent.title = self.btns.parent:Add("DLabel")
		self.btns.parent.title:SetSize(ScrW(), 64)
		self.btns.parent.title:SetFont("nutTitleFont")
		self.btns.parent.title:SetText(L2("schemaName") or SCHEMA.name or L"unknown")
		self.btns.parent.title:SizeToContents()
		self.btns.parent.title:SetPos(0,64)
		self.btns.parent.title:CenterHorizontal()
		self.btns.parent.title:SetTextColor(color_white)
		self.btns.parent.title:SetZPos(100)
		self.btns.parent.title:SetAlpha(0)
		self.btns.parent.title:AlphaTo(255, fadeSpeed, 1.5 * fadeSpeed, function()
			self.darkness:AlphaTo(0, 0.75 * fadeSpeed, 0, function()
				self.darkness:SetZPos(-99)
			end)
		end)
		self.btns.parent.title:SetExpensiveShadow(2, Color(0, 0, 0, 200))

    self.btns.list["Manage Characters"] = {
      open = function(panel)
				local shouldCharsDraw = #nut.characters > 0
				local x,y = panel.titleNode:GetPos()
				panel.createChar = panel:Add("DButton")
				panel.createChar:SetText("Create new character")
				panel.createChar:SetFont("nutSmallFont")
				panel.createChar:SetSize(180,40)
				panel.createChar:SetPos(25 + panel.titleNode:GetWide(), y)
				panel.createChar:SetColor(color_white)
				panel.createChar.overwriteDrawColor = nil
				panel.createChar.SHRINK = 1
				panel.createChar.EXPAND = 2
				function panel.createChar:GetColor() --Get/Set Color funcs (To work with the coloto func)
					return self.overwriteDrawColor
				end
				function panel.createChar:SetColor(col)
					self.overwriteDrawColor = col
				end
				function panel.createChar:Paint(w,h)
					if self:IsHovered() then
						draw.RoundedBox(4,0,0,w,h,Color(43,43,43))
					else
						draw.RoundedBox(4,0,0,w,h,Color(40,40,40))
					end

					if self.overwriteDrawColor then
						draw.RoundedBox(4,0,0,w,h,self.overwriteDrawColor)
					end
				end
				function panel.createChar:TweenWidthTo(w,way,callback)
					if not w then return end

					local tween = Derma_Anim("TweenWidth",self,function(pnl,anim,dt,data)
						if way == 1 then
							pnl:SetWide(data - w * dt^2)
						elseif way == 2 then
							pnl:SetWide(data + w * dt^2)
						end
					end)

					tween:Start(0.2, self:GetWide())
					function self:Think()
						if tween:Active() then
							tween:Run()
						end
					end

					if callback then
						timer.Simple(0.2, function()
							callback()
						end)
					end
				end
				function panel.createChar:CharLimit()
					self:TweenWidthTo(200, self.EXPAND, function()
						self:SetText("You reached the maximum number of characters")
					end)

					self.overwriteDrawColor = Color(43,43,43)
					self:ColorTo(Color(225,75,75),0.1,0)
					self:ColorTo(Color(40,40,40),0.5,0.8,function()
						self.overwriteDrawColor = nil
					end)

					timer.Simple(2, function()
						self:TweenWidthTo(180, self.SHRINK)
						self:SetText("Create Characters")
					end)
				end
				function panel.createChar.DoClick(this)
					if this.clickCooldown and CurTime() < this.clickCooldown then --Checking for cooldown
						return
					end
					this.clickCooldown = CurTime() + 5

					if #nut.characters >= nut.config.get("maxChars",5) then
						this:CharLimit()
						return
					end

					self.cc = vgui.Create("nutCharCreateReskined")
					self.cc:SetBackPanel(self)
				end

				if shouldCharsDraw then
					panel.scroll = panel.inner:Add("DScrollPanel") --List for the characters to be in
					panel.scroll:SetSize(panel.inner:GetSize())
					panel.list = panel.scroll:Add("DIconLayout")
					panel.list:SetSize(panel.scroll:GetWide()-10, panel.scroll:GetTall()-40)
					panel.list:SetPos(5,0)
					panel.list:SetSpaceY(5)

					local function getFaction(name)
						for k,v in pairs(nut.faction.indices) do
							if v.name:lower() == name:lower() then
								return v
							end
						end
					end

					for k,v in pairs(nut.characters) do
						local char = nut.char.loaded[v]
						local fac = getFaction(char.vars.faction)

						if not fac then
							MsgC(Color(255,75,75), "Character '" .. char.vars.name .. "' is using an unknown faction called '" .. char.vars.faction .. "'\n")
							continue
						end

						local c = panel.list:Add("DPanel")
						c:SetSize(panel.list:GetWide(), 250)
						local gradient = nut.util.getMaterial("vgui/gradient-u")
						function c:Paint(w,h)
							draw.RoundedBox(4,0,0,w,h,Color(40,40,40))

							if fac then
								surface.SetMaterial(gradient)
								surface.SetDrawColor(Color(fac.color.r, fac.color.g, fac.color.b, 5))
								surface.DrawTexturedRect(0,0,w,h/6)
							end
						end

						c.faction = c:Add("DLabel")
						c.faction:SetText(fac.name)
						c.faction:SetFont("nutSmallFont")
						c.faction:SetColor(color_white)
						c.faction:SizeToContents()
						c.faction:SetPos(c:GetWide()-c.faction:GetWide()-30, 20)

						c.mdlbg = c:Add("DPanel")
						c.mdlbg:SetSize(c:GetTall()+10, c:GetTall())
						function c.mdlbg:Paint(w,h)
							draw.RoundedBoxEx(4,0,0,w,h,Color(42,42,42),true,false,true)
							draw.RoundedBox(0,w-1,0,1,h,Color(60,60,60))
						end

						c.mdl = c.mdlbg:Add("DModelPanel")
            c.mdl:SetSize(c:GetTall(), c:GetTall())
            c.mdl:SetModel(char.vars.model)
						c.mdl:SetPos(10)
						c.mdl:CenterVertical()
            c.mdl:SetFOV(40)
						if c.mdl.Entity then
							local head = c.mdl.Entity:GetBonePosition(c.mdl.Entity:LookupBone("ValveBiped.Bip01_Head1"))
							c.mdl:SetLookAt(head)
							c.mdl.Entity:SetAngles(Angle(0,45,0))
							c.mdl.Entity:SetPos(Vector(0,0,8))
							function c.mdl:LayoutEntity()
							end
						end

						--Setting Nutscript stuff
						c.mdl.teamColor = team.GetColor(char:getFaction())
            c.mdl.Entity:SetSkin(char:getData("skin", 0))
            for n,g in pairs(char:getData("groups", {})) do
              c.mdl.Entity:SetBodygroup(n,g)
            end

						--Info
						c.info = c:Add("DPanel")
						c.info:SetSize(c:GetWide()-c.mdlbg:GetWide(), c:GetTall())
						c.info:SetPos(c.mdlbg:GetWide(),0)
						function c.info:Paint() end

						c.info.name = c.info:Add("DLabel")
						c.info.name:SetText(char.vars.name)
						c.info.name:SetFont("nutMediumFont")
						c.info.name:SetColor(color_white)
						c.info.name:SizeToContents()
						c.info.name:SetPos(20,10)

						function c.info:CreateDesc()
							if self.desc and IsValid(self.desc) then
								self.desc:Remove()
							end

							self.desc = c.info:Add("RichText")
							self.desc:SetPos(30,20 + c.info.name:GetTall())
							self.desc:InsertColorChange(255,255,255,255)
							self.desc:AppendText(char.vars.desc)
							self.desc:SetVerticalScrollbarEnabled(false)
							self.desc:SetSize(c.info:GetWide()-30,c.info:GetTall()/2)
							function self.desc:PerformLayout()
								self:SetFontInternal("nutSmallFont")
							end
						end
						c.info:CreateDesc()

						local x,y = c.info.desc:GetPos()
						c.info.money = c.info:Add("DLabel")
						c.info.money:SetText(nut.currency.symbol .. char.vars.money .. " ")
						c.info.money:SetFont("nutSmallFont")
						c.info.money:SetColor(color_white)
						c.info.money:SetPos(30, y + c.info.desc:GetTall() + 10)
						c.info.money:SizeToContents()

						c.info.btns = {}
						c.info.btns.edit = {
							label = "Edit",
							activeCol = Color(205, 217, 74),
							icon = "icon16/wrench.png",
							roundRad = 4,
							roundCorners = {false,false,false,true},
							click = function(btn)
								if not c.info.desc.isEditing then
									btn:SetText("Apply")
									btn.Active = true

									c.info.desc:SetVisible(false)
									c.info.desc.isEditing = true
									local x,y = c.info.desc:GetPos()

									local dt = c.info:Add("DTextEntry")
									dt:SetSize(c.info.desc:GetSize())
									dt:SetPos(x,y)
									dt:SetMultiline(true)
									dt:SetValue(char.vars.desc)

									c.info.desc.editTE = dt
								else
									btn:SetText("Edit")
									btn.Active = false

									local te = c.info.desc.editTE

									if te:GetValue() ~= char.vars.desc then --Check if it's different
										local descLen = nut.config.get("minDescLen")
										if #te:GetValue() < descLen then
											panel:Notify("Your description doesn't fit the criteria, it needs to be atleast " .. descLen .. " characters long.",5)
										else
											char.vars.desc = te:GetValue()
										end
									end

									te:Remove()
									c.info:CreateDesc()
								end
							end
						}
						c.info.btns.delete = {
							label = "Delete",
							activeCol = Color(198, 66, 66),
							icon = "icon16/cross.png",
							click = function(btn)
								local function close()
									btn.Active = false
									btn:SetText("Delete")

									btn.yes:AlphaTo(0,0.15)
									btn.no:AlphaTo(0,0.15,0,function()
										if btn and btn.yes and btn.no then
											btn.yes:Remove()
											btn.no:Remove()
										end
									end)
								end

								if not btn.Active then
									btn:SetText("Are you sure you want to delete this character ?")
									btn.Active = true

									--Yes Choice
									btn.yes = btn:Add("DButton")
									btn.yes:SetText("Yes")
									btn.yes:SetColor(color_white)
									btn.yes:SetFont("nutSmallFont")
									btn.yes:SetSize((btn:GetWide()/2)-2,btn:GetTall()-2)
									btn.yes:SetPos(1,1)
									btn.yes:SetAlpha(0)
									btn.yes:SetDisabled(true)
									btn.yes:AlphaTo(255,0.15,1,function()
										btn.yes:SetDisabled(false)
									end)
									function btn.yes:Paint(w,h)
										if self:IsHovered() then
											draw.RoundedBox(0,0,0,w,h,Color(33,33,33))
										else
											draw.RoundedBox(0,0,0,w,h,Color(30,30,30))
										end
									end
									function btn.yes.DoClick()
										if LocalPlayer():getChar() and LocalPlayer():getChar():getID() == char:getID() then
											panel:Notify("You are currently using this character.")
											close()
											return
										end

										netstream.Start("charDel", char:getID())
										nut.characters[k] = nil

										local wp = self.makeWorkPanel()
										wp:SetTitle("Manage Characters")
										self.btns.list["Manage Characters"].open(wp)
									end

									--No Choice
									btn.no = btn:Add("DButton")
									btn.no:SetText("No, Cancel")
									btn.no:SetColor(color_white)
									btn.no:SetFont("nutSmallFont")
									btn.no:SetSize((btn:GetWide()/2)-1,btn:GetTall()-2)
									btn.no:SetPos(2+btn.yes:GetWide(),1)
									btn.no:SetAlpha(0)
									btn.no:SetDisabled(true)
									btn.no:AlphaTo(255,0.15,1,function()
										btn.no:SetDisabled(false)
									end)
									function btn.no:Paint(w,h)
										if self:IsHovered() then
											draw.RoundedBox(0,0,0,w,h,Color(33,33,33))
										else
											draw.RoundedBox(0,0,0,w,h,Color(30,30,30))
										end
									end
									function btn.no:DoClick()
										close()
									end
								end
							end
						}

						local count = 0
						for _,info in pairs(c.info.btns) do
							local w = c.info:GetWide()/table.Count(c.info.btns)

							local b = c.info:Add("DButton")
							b:SetText(info.label)
							b:SetFont("nutSmallFont")
							b:SetColor(color_white)
							b:SetSize(w, 30)
							b:SetPos(w * count, c.info:GetTall()-b:GetTall())
							if info.icon then
								b:SetIcon(info.icon)
							end
							function b:Paint(w,h)
								local round = 0
								local corners = {false,false,false,false}
								if (info.roundRad and info.roundCorners and #info.roundCorners == 4) == true then
									round = info.roundRad
									corners = info.roundCorners
								end

								if self:IsHovered() then
									draw.RoundedBoxEx(round,0,0,w,h,info.activeCol,corners[1],corners[2],corners[3],corners[4])
									draw.RoundedBoxEx(round,1,1,w-2,h-2,Color(45,45,45),corners[1],corners[2],corners[3],corners[4])
								else
									draw.RoundedBoxEx(round,0,0,w,h,Color(45,45,45),corners[1],corners[2],corners[3],corners[4])
								end

								if self.Active then
									draw.RoundedBoxEx(round,0,0,w,h,info.activeCol,corners[1],corners[2],corners[3],corners[4])
								end
							end
							function b:DoClick()
								for _,i in pairs(c.info.btns) do --Check if any other button is active, return if yes
									if i.node ~= self and i.node.Active then
										return
									end
								end

								info.click(self)
							end

							info.node = b
							count = count + 1
						end
					end
				end
      end,
      vCenter = 0.55
    }
    self.btns.list["Play"] = {
      open = function(panel)
        local shouldDraw = (#nut.characters > 0 and hook.Run("ShouldMenuButtonShow", "load") ~= false)

        if shouldDraw then
          panel.scroll = panel.inner:Add("DScrollPanel")
          panel.scroll:SetSize(panel.inner:GetSize())
          panel.list = panel.scroll:Add("DIconLayout")
          panel.list:SetSize(panel.scroll:GetWide()-10, panel.scroll:GetTall()-10)
          panel.list:SetPos(5,5)
          panel.list:SetSpaceX(3)
          panel.list:SetSpaceY(3)

          for k,v in pairs(nut.characters) do
            local char = nut.char.loaded[v]
            local c = panel.list:Add("DPanel")
            c:SetSize(190,210)
            c:SetAlpha(0)
            c:AlphaTo(255,0.2,0.2)
            function c:Paint(w,h)
              draw.RoundedBox(4,0,0,w,h,Color(42,42,42))
            end

            c.mdlbg = c:Add("DPanel")
            c.mdlbg:SetSize(c:GetWide(), c:GetTall()-50)
            c.mdlbg:SetPos(0,20)
            function c.mdlbg:Paint(w,h)
              draw.RoundedBox(0,0,0,w,h,Color(38,38,38))
            end

            c.mdl = c.mdlbg:Add("DModelPanel")
            c.mdl:SetSize(c.mdlbg:GetSize())
            c.mdl:SetModel(char.vars.model)
            c.mdl:Center()
            c.mdl:SetFOV(40)
            local head = c.mdl.Entity:GetBonePosition(c.mdl.Entity:LookupBone("ValveBiped.Bip01_Head1"))
            c.mdl:SetLookAt(head)
            c.mdl.Entity:SetAngles(Angle(0,45,0))
            c.mdl.Entity:SetPos(Vector(0,0,8))
            function c.mdl:LayoutEntity()
            end

            --Setting Nutscript stuff
            c.mdl.teamColor = team.GetColor(char:getFaction())
            c.mdl.Entity:SetSkin(char:getData("skin", 0))
            for n,g in pairs(char:getData("groups", {})) do
              c.mdl.Entity:SetBodygroup(n,g)
            end

            c.name = c:Add("DLabel")
            c.name:SetText(char.vars.name)
            c.name:SetFont("nutSmallFont")
            c.name:SizeToContents()
            c.name:SetColor(color_white)
            c.name:CenterHorizontal()

            c.choose = c:Add("DButton")
            c.choose:SetText("Choose")
            c.choose:SetFont("nutSmallFont")
            c.choose:SetColor(color_white)
            c.choose:SetSize(c:GetWide(),30)
            c.choose:Dock(BOTTOM)
            function c.choose:Paint(w,h)
              if self:IsHovered() then
                draw.RoundedBoxEx(4,0,0,w,h,Color(57, 233, 90),false,false,true,true)
              else
                draw.RoundedBoxEx(4,0,0,w,h,Color(47, 223, 80),false,false,true,true)
              end
            end

            function c.choose.DoClick()
              local status, result = hook.Run("CanPlayerUseChar", LocalPlayer(), nut.char.loaded[v])
              if status == false then
                if (result:sub(1, 1) == "@") then
                  panel:Notify(result:sub(2))
                else
                  panel:Notify(result)
                end

                return
              end

              if LocalPlayer():getChar() and v == LocalPlayer():getChar():getID() then --Check if player is already using this character
                panel:Notify("You're already using this character.")
                return
              end

              self.darkness:SetZPos(999)
              self.darkness:AlphaTo(255,0.8,0,function()
                self:Remove()

                local darkness = vgui.Create("DPanel")
								darkness:SetZPos(999)
								darkness:SetSize(ScrW(), ScrH())
								darkness.Paint = function(this, w, h)
									surface.SetDrawColor(0, 0, 0)
									surface.DrawRect(0, 0, w, h)
								end

                local curChar = LocalPlayer():getChar()
                if curChar and curChar:getID() == v then
                  panel:Notify("You're already using this character.")
                end

                netstream.Hook("charLoaded", function()
                  if (IsValid(darkness)) then
                    darkness:AlphaTo(0, 5, 0.5, function()
                      darkness:Remove()
                    end)
                  end

                  if curChar ~= v then
                    hook.Run("CharacterLoaded", nut.char.loaded[v])
                  end
                end)

                netstream.Start("charChoose", v)
              end)
            end
          end
        end
      end,
      vCenter = 0.45
    }

		--Leave/Disconnect Button
    self.leave = self.btns.parent:Add("DButton") --Leave Button
		self.leave:SetZPos(5)
    self.leave:SetText("Leave")
    self.leave:SetFont("nutMediumFont")
    self.leave:SetColor(color_white)
		self.leave:SetSize(0,50)
		self.leave:Dock(BOTTOM)
    self.leave.gOff = 15
    function self.leave.DoClick()
      self.btns.parent.title:AlphaTo(0,0.2)
			self.darkness:SetZPos(999)
      self.darkness:AlphaTo(255, 0.5, 0.2, function()
        RunConsoleCommand("disconnect")
      end)
    end
    function self.leave:Paint(w,h)
      if self:IsHovered() then
        draw.RoundedBox(0,0,0,w,h,Color(235,60,60))
        surface.SetDrawColor(255,255,255)
        surface.DrawLine(15+self.gOff, 5, 5+self.gOff,h/2)
        surface.DrawLine(15+self.gOff, h-5, 5+self.gOff,h/2)
      else
        draw.RoundedBox(0,0,0,w,h,Color(31,23,23))
      end
    end
    function self.leave:OnCursorEntered()
      local mattr = Derma_Anim("mattr",self,function(pnl,anim,dt,data)
        pnl.gOff = pnl.gOff - (0.5 * dt)
      end)

      mattr:Start(0.2)
      function self:Think()
        if mattr:Active() then
          mattr:Run()
        end
      end
    end
    function self.leave:OnCursorExited()
      self.gOff = 15
      local mattl = Derma_Anim("mattl",self,function(pnl,anim,dt,data)
        pnl.gOff = pnl.gOff + (0.5 * dt)
      end)
      mattl:Start(0.2)

      function self:Think()
        if mattl:Active() then
          mattl:Run()
        end
      end
    end

    function self.makeWorkPanel()
      if self.wp and IsValid(self.wp) then
        self.wp:Remove()
      end

      self.wp = self:Add("DPanel")
      self.wp:SetSize(ScrW()-self.btns.parent:GetWide(),ScrH())
      self.wp:SetPos(self.btns.parent:GetWide(),0)
      self.wp:SetAlpha(0)
      self.wp:AlphaTo(255,0.2)
      function self.wp:Paint(w,h)
        draw.RoundedBox(0,0,0,w,h,Color(33,33,33))
        draw.RoundedBox(0,0,0,w,3,nut_color)
      end
      function self.wp:SetTitle(t)
        self.title = t

        self.titleNode = self:Add("DLabel")
        self.titleNode:SetText(t)
        self.titleNode:SetFont("nutBigFont")
        self.titleNode:SetColor(color_white)
        self.titleNode:SizeToContents()
        self.titleNode:SetPos(15, 30)
        self.titleNode:SetAlpha(0)
        self.titleNode:AlphaTo(255,0.15)

        local titleDep = Derma_Anim("init_subtitle", self.titleNode, function(pnl,anim,dt,data)
          pnl:SetPos(-pnl:GetWide() + ((pnl:GetWide()+15) * dt^2), 30)
        end)

        titleDep:Start(0.15)
        function self.titleNode:Think()
          if titleDep:Active() then
            titleDep:Run()
          end
        end
      end
      function self.wp:Notify(t,time)
				time = time or 2

        if self.notif and IsValid(self.notif) then
          self.notif:Remove()
          if timer.Exists("f1_workpanel_notif_timer") then timer.Remove("f1_workpanel_notif_timer") end
        end

        self.notif = self:Add("DPanel")
        self.notif:SetSize(self:GetWide(),40)
        function self.notif:Paint(w,h)
          draw.RoundedBox(0,0,0,w,h,nut_color)
        end

        self.notif.lbl = self.notif:Add("DLabel")
        self.notif.lbl:SetText(t)
        self.notif.lbl:SetFont("nutMediumFont")
        self.notif.lbl:SetColor(color_white)
        self.notif.lbl:SizeToContents()
        self.notif.lbl:Center()

        local dep = Derma_Anim("dep_notification", self.notif, function(pnl,anim,dt,data)
          local ph = pnl:GetParent():GetTall()
          pnl:SetPos(0, ph - pnl:GetTall() * dt^2)
        end)

        dep:Start(0.2)
        function self.notif:Think()
          if dep:Active() then
            dep:Run()
          end
        end

        timer.Create("f1_workpanel_notif_timer",time,1,function()
          if self and self.notif and IsValid(self.notif) then
            self.notif:AlphaTo(0,0.15,0,function()
              self.notif:Remove()
            end)
          end
        end)
      end

      self.wp.inner = self.wp:Add("DPanel")
      self.wp.inner:SetSize(self.wp:GetWide(),self.wp:GetTall()-80)
      self.wp.inner:SetPos(0,80)
      function self.wp.inner:Paint() end

      return self.wp
    end

    local active
    for k,v in pairs(self.btns.list) do
      local b
      self.btns.nodes[k] = self.btns.parent:Add("DButton")
      b = self.btns.nodes[k]
      b:SetText(k)
      b:SetFont("nutMediumFont")
      b:SetColor(color_white)
      b:SetSize(self.btns.parent:GetWide(),55)
      b:CenterVertical(v.vCenter)
      function b:Paint(w,h)
        if self:IsHovered() or self == active then
          draw.RoundedBox(0,0,0,w,h,Color(31,31,31))
        else
          draw.RoundedBox(0,0,0,w,h,Color(28,28,28))
        end

        if active == self then
          draw.RoundedBox(0,0,0,5,h,nut_color)
        end
      end
      function b.DoClick(this)
        if active == this then
          return
        end

        active = this

        local wp = self.makeWorkPanel()
        wp:SetTitle(k)

        v.open(wp)
      end

      timer.Simple(2 * fadeSpeed, function()
        local init = Derma_Anim("init_button", b, function(pnl,anim,dt,data)
          pnl:SetPos(-pnl:GetWide() + (pnl:GetWide() * dt^2), data)
        end)

        local x,y = b:GetPos()

        init:Start(0.3, y)
        function b:Think()
          if init:Active() then
            init:Run()
          end
        end
      end)
    end
  end

  function PANEL:playMusic()
		if (nut.menuMusic) then
			nut.menuMusic:Stop()
			nut.menuMusic = nil
		end

		timer.Remove("nutMusicFader")

		local source = nut.config.get("music", ""):lower()

		if (source:find("%S")) then
			local function callback(music, errorID, fault)
				if (music) then
					music:SetVolume(0.5)

					nut.menuMusic = music
					nut.menuMusic:Play()
				else
					MsgC(Color(255, 50, 50), errorID.." ")
					MsgC(color_white, fault.."\n")
				end
			end

			if (source:find("http")) then
				sound.PlayURL(source, "noplay", callback)
			else
				sound.PlayFile("sound/"..source, "noplay", callback)
			end
		end

		for k, v in ipairs(engine.GetAddons()) do
			if (v.wsid == "207739713" and v.mounted) then
				return
			end
		end

		Derma_Query(L"contentWarning", L"contentTitle", L"yes", function()
			gui.OpenURL("http://steamcommunity.com/sharedfiles/filedetails/?id=207739713")
		end, L"no")
	end


	function PANEL:OnRemove()
		if self.cc and IsValid(self.cc) then
			self.cc:Remove()
		end

		if (nut.menuMusic) then
			local fraction = 1
			local start, finish = RealTime(), RealTime() + 10

			timer.Create("nutMusicFader", 0.1, 0, function()
				if (nut.menuMusic) then
					fraction = 1 - math.TimeFraction(start, finish, RealTime())
					nut.menuMusic:SetVolume(fraction * 0.5)

					if (fraction <= 0) then
						nut.menuMusic:Stop()
						nut.menuMusic = nil

						timer.Remove("nutMusicFader")
					end
				else
					timer.Remove("nutMusicFader")
				end
			end)
		end
	end

	function PANEL:Paint(w, h)
    draw.RoundedBox(0,0,0,w,h,Color(25,25,25,235))
    nut.util.drawBlur(self, 10)
	end
vgui.Register("nutCharMenu", PANEL, "EditablePanel")
